class hardware::fixes {
	case $::hostname {
		bm-bl1,bm-bl2,bm-bl3,bm-bl4,bm-bl5,bm-bl6,bm-bl7,bm-bl8,bm-bl9,bm-bl10,bm-bl11,bm-bl12,bm-bl13,bm-bl14,lobos,villa: {
			concat::fragment { 'dsa-puppet-stuff--hp-health':
				target => '/etc/cron.d/dsa-puppet-stuff',
				#order  => '100',
				content  => @(EOF)
					@hourly root  (for i in `seq 1 5`; do timeout 25 hpasmcli -s help && break; sleep 5; service hp-health stop; sleep 5; service hp-health start; sleep 10; done) > /dev/null 2>/dev/null
					| EOF
			}
		}
	}
}
