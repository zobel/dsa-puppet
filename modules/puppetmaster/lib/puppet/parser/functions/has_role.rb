module Puppet::Parser::Functions
  newfunction(:has_role, :type => :rvalue) do |args|
      role = args[0]
      roles = lookupvar('site::roles')
      fqdn = lookupvar('fqdn')
      if not roles.include?(role)
        err "Failed to look up missing role #{role}"
        return false
      end
      case roles[role]
        when Hash then roles[role].include?(fqdn)
        else roles[role].map{ |k|
               case k
                 when Hash then k.keys.first
                 else k
               end
             }.include?(fqdn)
      end
  end
end
