define roles::mirror_health (
	$check_hosts    = [],
	$check_service  = '',
	$url            = '',
	$health_url     = '',
	$check_interval = 60,
) {
	ensure_packages(['python3-requests'], { ensure => 'installed' })

	if !defined(File['/usr/local/sbin/mirror-health']) {
	        file { '/usr/local/sbin/mirror-health':
		        source => 'puppet:///modules/roles/mirror_health/mirror-health',
		        owner  => 'root',
		        group  => 'root',
		        mode   => '0555',
                        notify  => Service["mirror-health-${check_service}"],
                }
        }

        file { "/etc/systemd/system/mirror-health-${check_service}.service":
		owner   => 'root',
		group   => 'root',
		mode    => '0444',
                content => template('roles/mirror-health.service.erb'),
                notify  => [Exec['systemctl daemon-reload'], Service["mirror-health-${check_service}"]],
        }

        file { "/etc/systemd/system/multi-user.target.wants/mirror-health-${check_service}.service":
		ensure => 'link',
		target => "../mirror-health-${check_service}.service",
		notify => Exec['systemctl daemon-reload'],
	}

	service { "mirror-health-${check_service}":
		require => Exec['systemctl daemon-reload'],
		ensure  => running,
        }
}
