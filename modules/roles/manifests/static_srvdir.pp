class roles::static_srvdir {
	file { '/srv/static.debian.org':
		ensure => directory,
		mode   => '0755',
		owner  => 'staticsync',
		group  => 'staticsync',
	}

	file { '/srv/static.debian.org/.nobackup':
		content => "",
	}
}
