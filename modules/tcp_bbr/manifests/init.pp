class tcp_bbr {
	# this is included by all hosts,
	# so filtering needs to happen here.

	if versioncmp($::lsbmajdistrelease, '9') >= 0 {
		if
		   has_role('api.ftp-master') or
		   has_role('bacula_storage') or
		   has_role('bugs_master') or
		   has_role('bugs_mirror') or
		   has_role('debian_mirror') or
		   has_role('debug_mirror') or
		   has_role('dgit_git') or
		   has_role('ftp.upload.d.o') or
		   has_role('ftp_master') or
		   has_role('git_master') or
		   has_role('historical_master') or
		   has_role('historical_mirror') or
		   has_role('ports_master') or
		   has_role('ports_mirror') or
		   has_role('postgres_backup_server') or
		   has_role('postgresql_server') or
		   has_role('security_master') or
		   has_role('security_mirror') or
		   has_role('security_upload') or
		   has_role('ssh.upload.d.o') or
		   has_role('static_master') or
		   has_role('static_mirror') or
		   has_role('static_source') or
		   has_role('syncproxy') or
		   has_role('www_master') or
		   false {

			site::linux_module { 'tcp_bbr': }
			site::linux_module { 'sch_fq': }

			site::sysctl { 'puppet-net_core_default_qdisc':
				key   => 'net.core.default_qdisc',
				value => 'fq',
			}
			site::sysctl { 'puppet-net_ipv4_tcp_congestion_control':
				key   => 'net.ipv4.tcp_congestion_control',
				value => 'bbr',
			}
		}
	}
}
